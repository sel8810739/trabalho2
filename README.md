# Trabalho 2

**Instruções:** Executar o main.py para verificar o código.

Há duas opções: Cadastrar um usuário, para isso fornecer nome e senha. Ou logar, para isso fornecer nome e senha.

A verificação de senha buscará por pelo menos um número para aumentar a segurança.

A criptografia da senha acontece pela conversão dos caracteres para os respectivos valores na tabela ASCII