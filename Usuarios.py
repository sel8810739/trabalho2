import re

class Usuario:
    def __init__(self, nomeDoUsuario, senha) -> None:
        self._nomeDoUsuario = nomeDoUsuario
        self._senha = senha
        pass

class adicionaUsuario(Usuario):
    def __init__(self, nomeDoUsuario, senha) -> None:
        super().__init__(nomeDoUsuario, senha)

    def verificaSenha(self) -> bool:
        regex = r'\d'
        for i in range(3):
            if re.search(regex, self._senha):
                print("Senha válida")
                return True
            else:
                print("Senha inválida")
                self._senha = input("Digite a senha novamente: ")
                continue
        return False

    def adicionaUsuario(self):
        bool = self.verificaSenha()

        if bool:
            linha = self.Criptografia()
            with open("usuarios.txt", "a") as arquivo:
                arquivo.write(linha + "\n")
        else:
            print("Tentativas esgotadas")

    def Criptografia(self) -> str:
        senha_ascii = [str(ord(caractere)) for caractere in self._senha]

        linha = ''.join(self._nomeDoUsuario) + '-' + ' '.join(senha_ascii)
        return linha

class logIn(Usuario):
    def __init__(self, nomeDoUsuario, senha) -> None:
        super().__init__(nomeDoUsuario, senha)
        self._nomeDoUsuario = nomeDoUsuario
        self._senha = senha

    def getUsuarios(self) -> list:
        linhas = []
        try:
            with open("usuarios.txt", "r") as arquivo:
                for linha in arquivo:
                    linhas.append(linha.strip())
            return linhas
        except:
            print("Sem usuários cadastrados")

    def verificaNome(self):
        listaDeUsuarios = self.getUsuarios()
        self.listaDeUsuarios = listaDeUsuarios
        i = 0
        for linha in listaDeUsuarios:
            i += 1
            linha = linha.split('-')
            if linha[0] == self._nomeDoUsuario:
                self._index = i
                self.verificaSenha()
                break
            else:
                break

    def verificaSenha(self):
        senha = self.Desriptografia()
        print(senha, self._senha)
        if senha == self._senha:
            print("Logado com sucesso")
        else:
            print("Senha incorreta")

    def Desriptografia(self) -> str:
        new_list = []
        for linha in self.listaDeUsuarios:
            new_list.append(linha)

        linhaDaSenha = new_list[self._index - 1].split('-')
        senha = linhaDaSenha[1].split(' ')
        senha = [chr(int(caractere)) for caractere in senha]
        senha = ''.join(senha)
        return senha